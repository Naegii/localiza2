
$('.slide').slick({
            infinite: false,
            slidesToShow: 1,
            initialSlide : 2,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            speed: 750,
            prevArrow: '<button type="button" class="setae"><img id="setaeweb" src="img/botaoe.png"></button>',
            nextArrow: '<button type="button" class="setad"><img id="setadweb" src="img/botaod.png"></button>'

        });

        $('.slide_ft').slick({
            infinite: false,
            slidesToShow: 4,
            initialSlide : 5,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            speed: 750,
            prevArrow: '<button type="button" class="setae"><img id="setaeft" src="img/botaoe.png"></button>',
            nextArrow: '<button type="button" class="setad"><img id="setadft" src="img/botaod.png"></button>'

        });

        $('.slide_art').slick({
         infinite: false,
         slidesToShow: 1,
         slidesToScroll: 1,
         dots: true,
         arrows: true,
         speed: 750,
         prevArrow: '<button type="button" class="setae"><img id="setaeart" src="img/botaoe.png"></button>',
         nextArrow: '<button type="button" class="setad"><img id="setadart" src="img/botaod.png"></button>'

     });


$("#list1").click(function(){
   $(".epi").fadeOut();
   $("#episode1").delay(500).fadeIn();
   $(".list").css('outline','inherit');
   $(this).css('outline', '1px solid #77b14a');
});

$("#list2").click(function(){
    $(".epi").fadeOut();
    $("#episode2").delay(500).fadeIn();
    $(".list").css('outline','inherit');
   $(this).css('outline', '1px solid #77b14a');
 });

 $("#list3").click(function(){
    $(".epi").fadeOut();
    $("#episode3").delay(500).fadeIn();
    $(".list").css('outline','inherit');
   $(this).css('outline', '1px solid #77b14a');
 });
/*
 $("#list4").click(function(){
    $(".epi").fadeOut();
    $("#episode4").delay(500).fadeIn();
    $(".list").css('outline','inherit');
   $(this).css('outline', '1px solid #77b14a');
 });

 $("#list5").click(function(){
    $(".epi").fadeOut();
    $("#episode5").delay(500).fadeIn();
    $(".list").css('outline','inherit');
   $(this).css('outline', '1px solid #77b14a');
 });*/

 $("#web1").click(function(){
   var aTag = $("#episodios");
   $('html,body').animate({scrollTop: aTag.offset().top},'slow');
   $(".epi").delay(500).fadeOut();
   $("#episode1").delay(1000).fadeIn();
   $(".list").css('outline','inherit');
   $('#list1').css('outline', '1px solid #77b14a');

 });

 $("#web2").click(function(){
   var aTag = $("#episodios");
   $('html,body').animate({scrollTop: aTag.offset().top},'slow');
   $(".epi").delay(500).fadeOut();
   $("#episode2").delay(1000).fadeIn();
   $(".list").css('outline','inherit');
   $('#list2').css('outline', '1px solid #77b14a');

 });


 $("#web3").click(function(){
   var aTag = $("#episodios");
   $('html,body').animate({scrollTop: aTag.offset().top},'slow');
   $(".epi").delay(500).fadeOut();
   $("#episode3").delay(1000).fadeIn();
   $(".list").css('outline','inherit');
   $('#list3').css('outline', '1px solid #77b14a');

 });


 $("#web4").click(function(){
   var aTag = $("#episodios");
   $('html,body').animate({scrollTop: aTag.offset().top},'slow');
   $(".epi").delay(500).fadeOut();
   $("#episode4").delay(1000).fadeIn();
   $(".list").css('outline','inherit');
   $('#list4').css('outline', '1px solid #77b14a');

 });


 $("#web5").click(function(){
   var aTag = $("#episodios");
   $('html,body').animate({scrollTop: aTag.offset().top},'slow');
   $(".epi").delay(500).fadeOut();
   $("#episode5").delay(1000).fadeIn();
   $(".list").css('outline','inherit');
   $('#list5').css('outline', '1px solid #77b14a');

 });


 $("#ft1").click(function () {
  $("#ft1").attr('src','img/foto1_360_sel.png');
  $("#ft2").attr('src','img/foto2_360.png');
  $("#ft3").attr('src','img/foto3_360.png');
  $("#ft4").attr('src','img/foto4_360.png');
  $("#ft5").attr('src','img/foto5_360.png');
  $("#ft6").attr('src','img/foto6_360.png');
  $("#ft7").attr('src','img/foto7_360.png');
  $("#ft8").attr('src','img/foto8_360.png');
  $("#ft9").attr('src','img/foto9_360.png');
  $("#ftx").attr('src','img/fotox_360.png');
  $("#fty").attr('src','img/fotoy_360.png');
   $("#foto360").fadeOut();
   $("#foto360 .iframe-responsive").delay(400).attr('src', 'https://www.youtube.com/embed/squKCFg7l5A?rel=0');
   $("#foto360").delay(500).fadeIn();
 });

 
 $("#ft2").click(function () {
  $("#ft1").attr('src','img/foto1_360.png');
  $("#ft2").attr('src','img/foto2_360_sel.png');
  $("#ft3").attr('src','img/foto3_360.png');
  $("#ft4").attr('src','img/foto4_360.png');
  $("#ft5").attr('src','img/foto5_360.png');
  $("#ft6").attr('src','img/foto6_360.png');
  $("#ft7").attr('src','img/foto7_360.png');
  $("#ft8").attr('src','img/foto8_360.png');
  $("#ft9").attr('src','img/foto9_360.png');
  $("#ftx").attr('src','img/fotox_360.png');
  $("#fty").attr('src','img/fotoy_360.png');
   $("#foto360").fadeOut();
   $("#foto360 .iframe-responsive").delay(400).attr('src', 'https://www.youtube.com/embed/BIt7lVcLp98?rel=0');
   $("#foto360").delay(500).fadeIn();
 });
 
 $("#ft3").click(function () {
  $("#ft1").attr('src','img/foto1_360.png');
  $("#ft2").attr('src','img/foto2_360.png');
  $("#ft3").attr('src','img/foto3_360_sel.png');
  $("#ft4").attr('src','img/foto4_360.png');
  $("#ft5").attr('src','img/foto5_360.png');
  $("#ft6").attr('src','img/foto6_360.png');
  $("#ft7").attr('src','img/foto7_360.png');
  $("#ft8").attr('src','img/foto8_360.png');
  $("#ft9").attr('src','img/foto9_360.png');
  $("#ftx").attr('src','img/fotox_360.png');
  $("#fty").attr('src','img/fotoy_360.png');
   $("#foto360").fadeOut();
   $("#foto360 .iframe-responsive").delay(400).attr('src', 'https://www.youtube.com/embed/ETwExrqbgMY?rel=0');
   $("#foto360").delay(500).fadeIn();
 });

 $("#ft4").click(function () {
  $("#ft1").attr('src','img/foto1_360.png');
  $("#ft2").attr('src','img/foto2_360.png');
  $("#ft3").attr('src','img/foto3_360.png');
  $("#ft4").attr('src','img/foto4_360_sel.png');
  $("#ft5").attr('src','img/foto5_360.png');
  $("#ft6").attr('src','img/foto6_360.png');
  $("#ft7").attr('src','img/foto7_360.png');
  $("#ft8").attr('src','img/foto8_360.png');
  $("#ft9").attr('src','img/foto9_360.png');
  $("#ftx").attr('src','img/fotox_360.png');
  $("#fty").attr('src','img/fotoy_360.png');
   $("#foto360").fadeOut();
   $("#foto360 .iframe-responsive").delay(400).attr('src', 'https://www.youtube.com/embed/dXSduLBa11I?rel=0');
   $("#foto360").delay(500).fadeIn();
 });

 $("#ft5").click(function () {
  $("#ft1").attr('src','img/foto1_360.png');
  $("#ft2").attr('src','img/foto2_360.png');
  $("#ft3").attr('src','img/foto3_360.png');
  $("#ft4").attr('src','img/foto4_360.png');
  $("#ft5").attr('src','img/foto5_360_sel.png');
  $("#ft6").attr('src','img/foto6_360.png');
  $("#ft7").attr('src','img/foto7_360.png');
  $("#ft8").attr('src','img/foto8_360.png');
  $("#ft9").attr('src','img/foto9_360.png');
  $("#ftx").attr('src','img/fotox_360.png');
  $("#fty").attr('src','img/fotoy_360.png');
   $("#foto360").fadeOut();
   $("#foto360 .iframe-responsive").delay(400).attr('src','https://www.youtube.com/embed/C6hpRXVNzd4?rel=0');
   $("#foto360").delay(500).fadeIn();
 });

 $("#ft6").click(function () {
  $("#ft1").attr('src','img/foto1_360.png');
  $("#ft2").attr('src','img/foto2_360.png');
  $("#ft3").attr('src','img/foto3_360.png');
  $("#ft4").attr('src','img/foto4_360.png');
  $("#ft5").attr('src','img/foto5_360.png');
  $("#ft6").attr('src','img/foto6_360_sel.png');
  $("#ft7").attr('src','img/foto7_360.png');
  $("#ft8").attr('src','img/foto8_360.png');
  $("#ft9").attr('src','img/foto9_360.png');
  $("#ftx").attr('src','img/fotox_360.png');
  $("#fty").attr('src','img/fotoy_360.png');
   $("#foto360").fadeOut();
   $("#foto360 .iframe-responsive").delay(400).attr('src', 'https://www.youtube.com/embed/YAaq2P1Aa3Y?rel=0');
   $("#foto360").delay(500).fadeIn();
 });

 $("#ft7").click(function () {
  $("#ft1").attr('src','img/foto1_360.png');
  $("#ft2").attr('src','img/foto2_360.png');
  $("#ft3").attr('src','img/foto3_360.png');
  $("#ft4").attr('src','img/foto4_360.png');
  $("#ft5").attr('src','img/foto5_360.png');
  $("#ft6").attr('src','img/foto6_360.png');
  $("#ft7").attr('src','img/foto7_360_sel.png');
  $("#ft8").attr('src','img/foto8_360.png');
  $("#ft9").attr('src','img/foto9_360.png');
  $("#ftx").attr('src','img/fotox_360.png');
  $("#fty").attr('src','img/fotoy_360.png');
  
   $("#foto360").fadeOut();
   $("#foto360 .iframe-responsive").delay(400).attr('src', 'https://www.youtube.com/embed/9zmHpVR0mio?rel=0');
   $("#foto360").delay(500).fadeIn();
 });

 $("#ft8").click(function () {
  $("#ft1").attr('src','img/foto1_360.png');
  $("#ft2").attr('src','img/foto2_360.png');
  $("#ft3").attr('src','img/foto3_360.png');
  $("#ft4").attr('src','img/foto4_360.png');
  $("#ft5").attr('src','img/foto5_360.png');
  $("#ft6").attr('src','img/foto6_360.png');
  $("#ft7").attr('src','img/foto7_360.png');
  $("#ft8").attr('src','img/foto8_360_sel.png');
  $("#ft9").attr('src','img/foto9_360.png');
  $("#ftx").attr('src','img/fotox_360.png');
  $("#fty").attr('src','img/fotoy_360.png');
   $("#foto360").fadeOut();
   $("#foto360 .iframe-responsive").delay(400).attr('src', 'https://www.youtube.com/embed/rcnG9Zk4nLQ?rel=0');
   $("#foto360").delay(500).fadeIn();
 });

 $("#ft9").click(function () {
  $("#ft1").attr('src','img/foto1_360.png');
  $("#ft2").attr('src','img/foto2_360.png');
  $("#ft3").attr('src','img/foto3_360.png');
  $("#ft4").attr('src','img/foto4_360.png');
  $("#ft5").attr('src','img/foto5_360.png');
  $("#ft6").attr('src','img/foto6_360.png');
  $("#ft7").attr('src','img/foto7_360.png');
  $("#ft8").attr('src','img/foto8_360.png');
  $("#ft9").attr('src','img/foto9_360_sel.png');
  $("#ftx").attr('src','img/fotox_360.png');
  $("#fty").attr('src','img/fotoy_360.png');
   $("#foto360").fadeOut();
   $("#foto360 .iframe-responsive").delay(400).attr('src', 'https://www.youtube.com/embed/Ms9hpfURi0Q?rel=0');
   $("#foto360").delay(500).fadeIn();
 });



$(function() {
  $('.iframe-responsive').Lazy();
  $('.textura1').Lazy();
  $('.textura2').Lazy();
  $('.casal').Lazy();
  $('.foto').Lazy();
  $('.eletroq').Lazy();
  $('.ft_vira').Lazy();

});

